import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_event.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_state.dart';
import 'package:keepdistance/screens/home_screen/home_screen.dart';
import 'package:keepdistance/screens/landing_screen/landing_screen.dart';
import 'package:keepdistance/screens/startup_screen.dart';

class AppBase extends StatelessWidget {
  const AppBase();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AuthBloc()..add(StartApp()),
      child: MaterialApp(
        title: 'Keep distance App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Color(0xFF3F25A5),
          accentColor: Color(0xFFF1E6FF),
          scaffoldBackgroundColor: Colors.white,
        ),
        home:
        BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state) =>
              Navigator.of(context).popUntil((route) => route.isFirst),
          listenWhen: (previous, current) =>
              previous is UnAuthenticated && current is Authenticated,
          builder: (BuildContext context, AuthState state) => state.when(
            loadingApp: () => StartupScreen(),
            authenticated: (user) => HomeScreen(user, ),
            unAuthenticated: () => LandingScreen(),
          ),
        ),
      ),
    );
  }
}
