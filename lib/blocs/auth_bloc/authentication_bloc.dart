import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_event.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_state.dart';
import 'package:keepdistance/models/user.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  @override
  // TODO: implement initialState
  AuthState get initialState => LoadingApp();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    yield* event.when(startApp: () async* {
      final currentUser = await FirebaseAuth.instance.currentUser();
      print(currentUser);
      if (currentUser == null) {
        yield UnAuthenticated();
        return;
      }
      final doc =
      await Firestore.instance.document('Users/${currentUser.uid}').get();
      if (!doc.exists) {
        yield UnAuthenticated();
        await FirebaseAuth.instance.signOut();
        return;
      }
      final user = User(doc.data['fullName'], doc.data['email']);
      yield Authenticated(user);
    }, authenticate: (user) async* {
      yield Authenticated(user);
    }, unauthenticate: () async* {
      await FirebaseAuth.instance.signOut();
//       await GoogleSignIn().disconnect().whenComplete(() async {
//         await GoogleSignIn().signOut();
//       });
      yield UnAuthenticated();
    });
  }

















































//  Future logout() async {
//     GoogleSignIn().onCurrentUserChanged.listen((
//        GoogleSignInAccount account) async {
//       print(account);
//
//      if (account != null) {
//        print("disconnect function");
//
//
//      } else {
//        await FirebaseAuth.instance.signOut();
//        GoogleSignIn().disconnect();
//
//      }
//    });}

}