import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:keepdistance/models/user.dart';
//abstract class AuthEvent{}
//class StartApp extends AuthEvent{}
//class Authenticate  extends AuthEvent{User user;}
//class UnAuthenticate extends AuthEvent{}

part 'authentication_event.freezed.dart';

@freezed
abstract class AuthEvent with _$AuthEvent {
  const factory AuthEvent.startApp() = StartApp;
  const factory AuthEvent.authenticate(User user) = Authenticate;
  const factory AuthEvent.unauthenticate() = Unauthenticate;
}
