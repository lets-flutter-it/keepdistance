import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:keepdistance/models/user.dart';

part 'authentication_state.freezed.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState.loadingApp() = LoadingApp;
  //todo
  const factory AuthState.authenticated(User user) = Authenticated;
  const factory AuthState.unAuthenticated() = UnAuthenticated;
}
