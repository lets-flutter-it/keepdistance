import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:keepdistance/models/user.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>['email']
);


Future<User> signInWithGoogle() async {

  print("in google signin");
  User user;
  await _googleSignIn.signIn().then((response) => {
    _googleSignIn.signInSilently(),
    user= User(response.displayName, response.email) ,
    print('inside await'),
    print(response)
  });

  return user;

}
void signOutGoogle() async{
  await googleSignIn.signOut();

  print("User Sign Out");
}
