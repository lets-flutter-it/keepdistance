import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_event.dart';
import 'package:keepdistance/models/user.dart';
import 'package:keepdistance/screens/home_screen/text_container.dart';
import 'package:keepdistance/screens/landing_screen/landing_screen.dart';
import 'package:keepdistance/screens/register_screen/widgets/resgister_screen_background.dart';
import 'package:keepdistance/widgets/text_field_container.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// ignore: must_be_immutable
class HomeScreen extends StatefulWidget {
   HomeScreen(this.user);
  final User user;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(

      body: Stack(
        children: <Widget>[
          Positioned(
            right: 0,
            top: 0,
            child: SafeArea(
              child: IconButton(
                onPressed: () =>
                    BlocProvider.of<AuthBloc>(context).add(Unauthenticate()),
                icon: Icon(
                  FontAwesomeIcons.signOutAlt,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
          ),

          RegisterBackground(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Welcome to  ",
                    style: GoogleFonts.pompiere(
                      textStyle: TextStyle(
                          color: Color(0xFF6F35A5),
                          letterSpacing: 1,
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    "Keep  ",
                    style: GoogleFonts.pompiere(
                      textStyle: TextStyle(
                          color: Color(0xFF6F35A5),
                          letterSpacing: 1,
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "D.  I.  S.  T.  A.  N.  C.  E",
                    style: GoogleFonts.pompiere(
                      textStyle: TextStyle(
                          color: Colors.purple[700],
                          letterSpacing: 2,
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(height: size.height * 0.01),
                CircleAvatar(
                  radius: 90,
                  backgroundImage: AssetImage(
                    "assets/images/male_avatar.png",
                  ),),
                  TextContainer(child: Row(children: <Widget>[ Text('Email address:   '),Text(widget.user.email),],),),
                  SizedBox(height: size.height * 0.03),
                  TextContainer(child: Row(children: <Widget>[ Text('Full Name:   '),Text(widget.user.fullName),],),),
//              SizedBox(height: size.height * 0.03),
//              TextContainer(),
//              SizedBox(height: size.height * 0.03),
//              TextContainer(),
                    ],
                  )

              ),
            ),
        ],
      ),
    );

  }
}
