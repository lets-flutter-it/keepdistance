import 'package:flutter/material.dart';


class TextContainer extends StatelessWidget {
  final Widget child;

  const TextContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(vertical: 3),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
