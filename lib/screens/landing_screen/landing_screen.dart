import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:keepdistance/screens/landing_screen/widgets/landing_body.dart';

class LandingScreen extends StatelessWidget{
  const LandingScreen();

  @override
  Widget build(BuildContext context) {
   return Scaffold(body: LandingBody());
  }
  
}