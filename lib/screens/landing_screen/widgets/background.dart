import 'package:flutter/material.dart';

class LandingBackground extends StatelessWidget {
  final Widget child;
  const LandingBackground({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size= MediaQuery.of(context).size;

    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset("assets/images/landing_top.png",
                width: size.width * 0.3),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Image.asset("assets/images/landing_bottom.png",
                width: size.width * 0.2),
          ),
          child ,
        ],
      ),
    );
  }

}
