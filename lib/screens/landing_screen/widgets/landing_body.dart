import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keepdistance/screens/landing_screen/widgets/background.dart';
import 'package:keepdistance/screens/login_screen/login_screen.dart';
import 'package:keepdistance/screens/register_screen/register_screen.dart';
import 'package:keepdistance/widgets/rounded_button.dart';

class LandingBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LandingBackground(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left:0, top: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Keep ",
                style: GoogleFonts.pompiere(
                  textStyle: TextStyle(
                      color: Color(0xFF6F35A5),
                      letterSpacing: 1,
                      fontSize: 40,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                "D.  I.  S.  T.  A.  N.  C.  E",
                style: GoogleFonts.pompiere(
                  textStyle: TextStyle(
                      color: Colors.purple[700],
                      letterSpacing: 2,
                      fontSize: 40,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: size.height * 0.05),
//              SvgPicture.asset(
//                "assets/icons/landing_page.svg",
//                height: size.height * 0.45,
//              ),
            Image.asset('assets/trail/landing_trail.png'),
              SizedBox(height: size.height * 0.05),
              RoundedButton(
                text: "LOGIN",
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
              RoundedButton(
                text: "Register Now",
                color: Theme.of(context).accentColor,
                textColor: Colors.black,
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterScreen(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
