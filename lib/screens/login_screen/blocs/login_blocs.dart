import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_event.dart';
import 'package:keepdistance/models/user.dart';
import 'package:keepdistance/screens/login_screen/blocs/login_events.dart';
import 'package:keepdistance/screens/login_screen/blocs/login_states.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_bloc.dart';
import 'package:keepdistance/util/validator.dart';

class LoginBloc extends Bloc<LoginEvent,LoginState>{
  LoginBloc(this.AuthBloc,);

  @override
  LoginState get initialState => LoginState.initial();
  final AuthBloc;
  final emailController = TextEditingController(),
      passwordController = TextEditingController();


  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    final email = emailController.text,
        password = passwordController.text;
    if (!Validator.validateEmail(email)) {
      emailController.clear();
      yield LoginState.loginError('Invalid Email');
      yield LoginState.initial();
      return;
    }
    if (!Validator.validatePassword(password)) {
      passwordController.clear();
      yield LoginState.loginError('Invalid Password');
      yield LoginState.initial();
      return;
    }
    final error = LoginState.loginError(" Invalid Email or Password");
    yield LoginState.loading();
    try {
      final result = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email, password: password);
      final uid= result.user.uid;
      final doc =await Firestore.instance.document('Users/$uid').get();
      if (!doc.exists) {
        await FirebaseAuth.instance.signOut();
        yield error;
      }
      final user = User(doc.data, email);
      AuthBloc.add(Authenticate(user));

    } catch (e) {
      print(e);
      yield error;
    }


      yield LoginState.initial();
  }}