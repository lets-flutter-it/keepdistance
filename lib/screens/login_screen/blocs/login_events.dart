import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_events.freezed.dart';

@freezed
abstract class LoginEvent with _$LoginEvent {
  const factory LoginEvent.loginWithEmail() = LoginWithEmail;
}
