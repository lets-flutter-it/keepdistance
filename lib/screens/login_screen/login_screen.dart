import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_bloc.dart';
import 'package:keepdistance/screens/login_screen/widgets/login_body.dart';

import 'blocs/login_blocs.dart';

//import 'package:flutter_auth/Screens/Login/components/body.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create:(_) => LoginBloc(BlocProvider.of<AuthBloc>(context)) ,
      child: Scaffold(
        body: LoginBody(),
      ),
    );
  }
}