import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:keepdistance/screens/login_screen/blocs/login_blocs.dart';
import 'package:keepdistance/screens/login_screen/blocs/login_events.dart';
import 'package:keepdistance/screens/login_screen/blocs/login_states.dart';
import 'package:keepdistance/screens/register_screen/first_register_screen.dart';
import 'package:keepdistance/screens/register_screen/register_screen.dart';
import 'package:keepdistance/widgets/already_registered_row.dart';
import 'package:keepdistance/widgets/custom_text_field.dart';
import 'package:keepdistance/widgets/rounded_button.dart';
import 'background.dart';

class LoginBody extends StatelessWidget {
const LoginBody({
Key key,
}) : super(key: key);

@override
Widget build(BuildContext context) {
  Size size = MediaQuery.of(context).size;
   final bloc =BlocProvider.of<LoginBloc>(context);
  return BlocListener<LoginBloc,LoginState>(
      listener:(context,state){
        if(state is LoginError) {
          Flushbar(
            title: 'Error',
            message: state.text,
            duration: Duration(seconds: 5),
            backgroundColor: Theme.of(context).primaryColor,

          ).show(context);

        }
      } ,
    child: Background(
      child: SingleChildScrollView(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            SizedBox(height: size.height * 0.03),

            SvgPicture.asset(
              "assets/icons/login_page.svg",
              height: size.height * 0.35,
            ),
            SizedBox(height: size.height * 0.03),
            CustomInputField(
              hintText: "Your Email",
              controller:bloc.emailController ,

            ),
            CustomInputField(
              icon: Icons.email,
              hintText: "Your Password",
              suffixIcon: Icons.visibility,
              controller:bloc.passwordController ,
              obscureText: true,
            ),

            BlocBuilder<LoginBloc,LoginState>(
              builder: (context, state) {
                if(state is Loading){

                  return CircularProgressIndicator(
                    backgroundColor: Colors.deepPurple,
                  );
                }
                else{ return RoundedButton(
                    text: "LOGIN",
                    press: () => bloc.add(LoginEvent.loginWithEmail()),
                  );}
              }
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyRegisteredRow (
              press: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return RegisterScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    ),
  );
}
}
