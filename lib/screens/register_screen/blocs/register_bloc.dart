import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_event.dart';
import 'package:keepdistance/models/user.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_event.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_states.dart';
import 'package:keepdistance/util/validator.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  @override
  RegisterState get initialState => InitialRegistration();

  RegisterBloc(this.authBloc);
  final AuthBloc authBloc;

  final emailController = TextEditingController(),
      passwordController = TextEditingController(),
      confirmPasswordController = TextEditingController(),
      fullNameController = TextEditingController();

  String email, password;

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterEmailAndPassword) {
      final email = emailController.text,
          password = passwordController.text,
          passwordConfirmation = confirmPasswordController.text;

      if (!Validator.validateEmail(email)) {
        emailController.clear();
        yield ErrorRegistration(text: 'Invalid Email');
        yield InitialRegistration();
        return;
      }
      if (!Validator.validatePassword(password)) {
        passwordController.clear();
        yield ErrorRegistration(text: 'Invalid Password');
        yield InitialRegistration();
        return;
      }
      if (passwordConfirmation != password) {
        confirmPasswordController.clear();
        yield ErrorRegistration(text: 'password mismatch');
        yield InitialRegistration();
        return;
      }

      this.email = email;
      this.password = password;
      yield InitialRegistration(firstStage: false);
    }

    if (event is RegisterUserDetails) {
      final fullName = fullNameController.text;
      if (!Validator.validateFullName(fullName)) {
        fullNameController.clear();
        yield ErrorRegistration(firstStage: false, text: 'Invalid full name!');
        yield InitialRegistration(firstStage: false);
        return;
      }
      try {
        final result =
            await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password,
        );
        final user = User(fullName, email);
        final uid = result.user.uid;
        await Firestore.instance.document('Users/$uid').setData({
          'fullName': fullName,
          'email': email,
        });

        authBloc.add(AuthEvent.authenticate(user));
      } catch (e) {
        print(e);
        yield ErrorRegistration(
          firstStage: false,
          text: "Email already in use",
        );
        yield InitialRegistration();
        return;
      }
    }
  }
}
