import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class RegisterEvent {}

class RegisterEmailAndPassword extends RegisterEvent {}

class RegisterUserDetails extends RegisterEvent {
  RegisterUserDetails(this.context);
  final BuildContext context;
}

class RegisterWithGmail extends RegisterEvent {


}

