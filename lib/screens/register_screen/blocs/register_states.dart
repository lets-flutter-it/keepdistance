import 'package:flutter/foundation.dart';

abstract class RegisterState {
  const RegisterState(this.firstStage);
  final bool firstStage;
}

class InitialRegistration extends RegisterState {
  InitialRegistration({bool firstStage = true}) : super(firstStage);
}

class LoadingRegistration extends RegisterState {
  LoadingRegistration({bool firstStage = true}) : super(firstStage);
}

class ErrorRegistration extends RegisterState {
  final String text;
  ErrorRegistration({
    bool firstStage = true,
    @required this.text,
  }) : super(firstStage);
}
