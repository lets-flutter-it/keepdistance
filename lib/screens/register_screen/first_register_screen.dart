import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keepdistance/screens/register_screen/widgets/First_register_screen_body.dart';

//
class FirstRegisterScreen extends StatelessWidget {
  const FirstRegisterScreen();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FirstRegisterBody(),
    );
  }
}
