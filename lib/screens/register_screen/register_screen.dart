import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepdistance/blocs/auth_bloc/authentication_bloc.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_bloc.dart';
import 'package:keepdistance/screens/register_screen/first_register_screen.dart';
import 'package:keepdistance/screens/register_screen/second_register_screen.dart';
import 'package:keepdistance/util/error_handler.dart';

import 'blocs/register_states.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen();

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => RegisterBloc(BlocProvider.of<AuthBloc>(context)),
        child: BlocConsumer<RegisterBloc, RegisterState>(
          listener: (_, state) => state is ErrorRegistration
              ? ErrorHandler.showError(context, state.text)
              : null,
          builder: (_, state) =>
              state.firstStage ? FirstRegisterScreen() : SecondRegisterScreen(),
        ),
      );
}
