import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_bloc.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_event.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_states.dart';
import 'package:keepdistance/screens/register_screen/widgets/resgister_screen_background.dart';
import 'package:keepdistance/widgets/custom_text_field.dart';
import 'package:keepdistance/widgets/rounded_button.dart';
import 'package:keepdistance/widgets/text_field_container.dart';

class SecondRegisterScreen extends StatefulWidget {
  @override
  _SecondRegisterScreenState createState() => _SecondRegisterScreenState();
}

class _SecondRegisterScreenState extends State<SecondRegisterScreen> {
  int group = 1;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final bloc = BlocProvider.of<RegisterBloc>(context);
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is ErrorRegistration) {}
      },
      child: Scaffold(
        body: RegisterBackground(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.height * 0.03),
                CustomInputField(
                  hintText: "Full Name",
                  controller: bloc.fullNameController,
                ),
                CustomInputField(
                  hintText: "user name",
                ),
                Container(

                  child: TextFieldContainer(
                    child: Row(
                      children: <Widget>[
//                        Text(
//                          "Gender:",
//                          style: TextStyle(fontSize: 20),
//                        ),
                        SizedBox(
                          width: 25,
                        ),
                        Text(
                          "Male",
                          style: TextStyle(fontSize: 20),
                        ),
                        Radio(
                            value: 1,
                            focusColor: Theme.of(context).primaryColor,
                            hoverColor: Theme.of(context).primaryColor,
                            groupValue: group,
                            onChanged: (T) {
                              print(T);
                              setState(() {
                                group = T;
                              });
                            }),
                        Text(
                          "Female",
                          style: TextStyle(fontSize: 20),
                        ),
                        Radio(
                            focusColor: Theme.of(context).primaryColor,
                            hoverColor: Theme.of(context).primaryColor,
                            value: 2,
                            groupValue: group,
                            onChanged: (T) {
                              print(T);
                              setState(() {
                                group = T;
                              });
                            })
                      ],
                    ),
                  ),
                ),
                BlocBuilder<RegisterBloc,RegisterState>(
                    builder:(context, state){
                      if(state is LoadingRegistration){
                        return CircularProgressIndicator(
                          backgroundColor: Colors.deepPurple,
                        );
                      }
                      else{
                        return RoundedButton(
                          text: "Create account",
                          press: () {

                            bloc.add(RegisterUserDetails(context));
//                    Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                      builder: (context) {
//                        return HomeScreen();
//                      },
//                    ),
//                  );
                          },
                        );
                      }
                    }
                ),
                SizedBox(height: size.height * 0.03),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
