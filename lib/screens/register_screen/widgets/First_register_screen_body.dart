import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:keepdistance/blocs/google_sign_in.dart';
import 'package:keepdistance/models/user.dart';
import 'package:keepdistance/screens/home_screen/home_screen.dart';
import 'package:keepdistance/screens/login_screen/login_screen.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_bloc.dart';
import 'package:keepdistance/screens/register_screen/blocs/register_event.dart';
import 'package:keepdistance/screens/register_screen/widgets/resgister_screen_background.dart';
import 'package:keepdistance/screens/register_screen/widgets/screen_spliter.dart';
import 'package:keepdistance/screens/register_screen/widgets/social_media_icons.dart';
import 'package:keepdistance/widgets/already_registered_row.dart';
import 'package:keepdistance/widgets/custom_text_field.dart';
import 'package:keepdistance/widgets/rounded_button.dart';

class FirstRegisterBody extends StatelessWidget {
  final User user;

  const FirstRegisterBody({Key key, this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final bloc = BlocProvider.of<RegisterBloc>(context);
    return RegisterBackground(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.025),
              Image.asset("assets/images/register_screen.png"),

              CustomInputField(
                hintText: "Your Email",
                controller: bloc.emailController,
              ),
              CustomInputField(
                icon: Icons.lock,
                hintText: "type new password",
                controller: bloc.passwordController,
                obscureText: true,
                suffixIcon: Icons.visibility,
              ),
              CustomInputField(
                icon: Icons.lock,
                hintText: "re-type password",
                controller: bloc.confirmPasswordController,
                obscureText: true,
                suffixIcon: Icons.visibility,
              ),
              RoundedButton(
                text: "NEXT",
                press: () => bloc.add(RegisterEmailAndPassword()),
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyRegisteredRow(
                login: false,
                press: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
              ScreenSplitter(),
              Container(
                height: size.height *0.07,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: <Widget>[

//                    Image.asset('assets/icons/twitter_logo.png'
//                      ,height: size.height *0.3, width: size.width *0.3,),
                    SizedBox(
                      height: size.height *0.23, width: size.width *0.23,
                      child: FlatButton(

                        onPressed: () {
                          signInWithGoogle().then((googleUser){
                            // googleUser;
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) {
                                  return HomeScreen(googleUser);
                                },
                              ),
                            );
                          });

                        },
                        child: Image.asset('assets/icons/google_logo.webp'
                          ,height: size.height *0.35, width: size.width *0.35,),
                      ),
                    ),
                    Image.asset('assets/icons/facebook_logo.png'
                      ,height: size.height *0.12, width: size.width *0.12,),

                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
