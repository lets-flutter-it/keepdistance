import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StartupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    // can be const

    body: Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
//            Colors.deepPurple[300],
            Colors.deepPurple[200],
            Colors.deepPurple[100],
            Colors.deepPurple[50],
            Colors.white70,
          ],
        ),
      ),
      // can be const (better for performance)
      child:  Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 250,
            ),
            Text(
              "Keep ",
              style: GoogleFonts.pompiere(
                textStyle: TextStyle(
                    color: Color(0xFF6F35A5),
                    letterSpacing: 1,
                    fontSize: 40,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "D.  I.  S.  T.  A.  N.  C.  E",
              style: GoogleFonts.pompiere(
                textStyle: TextStyle(
                    color: Color(0xFF6F35A5),
                    letterSpacing: 2,
                    fontSize: 40,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            CircularProgressIndicator(
              backgroundColor: Colors.deepPurple,
            ),
          ],
        ),
      ),
    ),
  );
}
