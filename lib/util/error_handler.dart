import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

abstract class ErrorHandler {
  static void showError(BuildContext context, String message) => Flushbar(
        title: 'Error',
        message: message,
        duration: Duration(seconds: 5),
        backgroundColor: Theme.of(context).primaryColor,
      ).show(context);
}
