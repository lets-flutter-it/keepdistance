import 'package:flutter/material.dart';
//import 'package:flutter_auth/constants.dart';


class AlreadyRegisteredRow extends StatelessWidget {
  final bool login;
  final Function press;
  const AlreadyRegisteredRow ({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "Don’t have an Account ? " : "Already have an Account ? ",
          style: TextStyle(color: Theme.of(context).primaryColor ,
          fontSize: 17),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? "Register" : "Login",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
        )
      ],
    );
  }
}