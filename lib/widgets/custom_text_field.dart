import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keepdistance/widgets/text_field_container.dart';

class CustomInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final IconData suffixIcon;
  final TextEditingController controller;
  final bool obscureText;

  const  CustomInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.controller, this.suffixIcon,
    this.obscureText=false,

  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(

      child: TextField(
        obscureText: obscureText,
        controller: controller,
        cursorColor: Theme.of(context).primaryColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: Theme.of(context).primaryColor,
          ),
          suffixIcon:Icon(suffixIcon),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}